﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/**
 * Exe.config values:
 * ExpectedHash
 * BadMailPath
 * 
 * Query values:
 * hash
 * */
public partial class GetBadMail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		string receivedHash = Request.QueryString["hash"];

		Response.ContentType = "text/plain";
		
		string expectedHash = ConfigurationManager.AppSettings["ExpectedHash"];

		// quiet exit should be more secure
		if (receivedHash != expectedHash)
			Response.End();

		
		string badMailFolderPath = ConfigurationManager.AppSettings["BadMailPath"];

		// get next bad mail
		if (Directory.Exists(badMailFolderPath) == false)
		{
			SendResponseFailure("Can't find folder " + badMailFolderPath);
			return;
		}

		string[] files = Directory.GetFiles(badMailFolderPath, "*.BAD");

		if(files.Length == 0)
			Response.End();
		
		// add filename
		Response.Write(files[0] + "\n");

		// add content
		string content = File.ReadAllText(files[0], System.Text.Encoding.UTF8);

		Response.Write(content);

		// send
		Response.End();
    }

	/// <summary> writes error message to response and closes it </summary>
	/// <param name="in_message">error message</param>
	private void SendResponseFailure(string in_message)
	{
		Response.Write(in_message);
		Response.End();
	}
}