﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/**
 * Exe.config values:
 * ExpectedHash
 * BadMailPath
 * 
 * Query values:
 * hash
 * filename
 * */
public partial class ClearBadMail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		string receivedHash = Request.QueryString["hash"];

		Response.ContentType = "text/plain";
		
		string expectedHash = ConfigurationManager.AppSettings["ExpectedHash"];

		// quiet exit should be more secure
		if(receivedHash != expectedHash)
			Response.End();

		string badMailFolderPath = ConfigurationManager.AppSettings["BadMailPath"];
		string requestedDeleteFile = Request.QueryString["filename"];
				
		// get next bad mail
		string fileWithExtBAD = Path.ChangeExtension(requestedDeleteFile, "BAD");
		string fileWithExtBDP = Path.ChangeExtension(requestedDeleteFile, "BDP");
		string fileWithExtBDR = Path.ChangeExtension(requestedDeleteFile, "BDR");


		Response.Write(fileWithExtBAD + "\n");
		Response.Write(fileWithExtBDP + "\n");
		Response.Write(fileWithExtBDR + "\n");

		// delete variations
		int numDeletedFiles = 0;
		numDeletedFiles += DeleteFile(fileWithExtBAD);
		numDeletedFiles += DeleteFile(fileWithExtBDP);
		numDeletedFiles += DeleteFile(fileWithExtBDR);

		Response.Write(numDeletedFiles);
		Response.End();
    }

	private int DeleteFile(string in_path)
	{
		try
		{
			if (File.Exists(in_path) == false)
			{
				Response.Write("File not found:  " + in_path + "\n");
				return 0;
			}

			File.Delete(in_path);
			return 1;
		}
		catch (Exception e)
		{
			Response.Write("Exception:  "+ e.ToString() + "\n");
		}

		return 0;
	}
}